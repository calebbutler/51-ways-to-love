#ifndef _MENU_H
#define _MENU_H

struct state;
struct game_data;

struct small_menu {
	int tx;
	int ty;
	int selected_entry;
	int amount_of_entries;
	char entries[30][8];
	void (*callbacks[30])(struct game_data *, struct state *);
};

void small_menu_add_entry(struct small_menu *sm, const char *string,
			  void (*callback)(struct game_data *game,
					   struct state *state));
void small_menu_handle_events(struct game_data *game, struct state *state);
void small_menu_draw(struct game_data *game, struct state *state);

#endif /* _MENU_H */
