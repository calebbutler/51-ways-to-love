#ifndef _GAME_H
#define _GAME_H

#include <SDL2/SDL.h>

#include <stdbool.h>

#include "draw.h"
#include "menu.h"
#include "textbox.h"

enum state_type {
	STATE_TYPE_SMALL_MENU,
	STATE_TYPE_TEXTBOX,
};

struct state {
	union {
		struct small_menu smenu;
		struct textbox textbox;
	} data;
	enum state_type type;
	bool is_updating;
	bool is_rendering;
};

struct game_data {
	struct state stack[8];
	struct state *stackp;
	struct draw_data dd;
	SDL_Event event;
	bool is_running;
};

bool game_init(struct game_data *game);
void game_event_handling(struct game_data *game);
void game_update(struct game_data *game);
void game_draw(struct game_data *game);
void game_cleanup(struct game_data *game);
void game_state_stack_unsafe_pop(struct game_data *game);
bool game_state_stack_safe_pop(struct game_data *game);
void game_state_stack_pop(struct game_data *game);
void game_state_stack_push(struct game_data *game);
void game_state_stack_update_and_render_top(struct game_data *game);

#endif /* _GAME_H */
