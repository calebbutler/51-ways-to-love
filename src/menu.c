#include "draw.h"
#include "menu.h"
#include "game.h"

void small_menu_add_entry(struct small_menu *sm, const char *string,
			  void (*callback)(struct game_data *game,
					   struct state *state))
{
	int i;

	if (sm->amount_of_entries > 30) {
		SDL_Log("Too many entries for small menu!\n");
	} else {
		for (i = 0; i < 8; i++) {
			sm->entries[sm->amount_of_entries][i] = string[i];
			if (string[i] == '\0')
				break;
		}
		sm->callbacks[sm->amount_of_entries] = callback;
		sm->amount_of_entries++;
	}
}


void small_menu_handle_events(struct game_data *game, struct state *state)
{
	if (game->event.type == SDL_KEYDOWN && game->event.key.repeat == 0) {
		switch (game->event.key.keysym.sym) {
		case SDLK_UP:
			if (state->data.smenu.selected_entry > 0)
				state->data.smenu.selected_entry--;
			break;
		case SDLK_DOWN:
			if (state->data.smenu.selected_entry + 1 <
			    state->data.smenu.amount_of_entries)
				state->data.smenu.selected_entry++;
			break;
		case SDLK_RIGHT:
			state->data.smenu.callbacks[
			    state->data.smenu.selected_entry](game, state);
			break;
		case SDLK_LEFT:
			if (game_state_stack_safe_pop(game))
				game_state_stack_update_and_render_top(game);
			break;
		}
	}
}

void small_menu_draw(struct game_data *game, struct state *state)
{
	int i;

	draw_rect(&game->dd, state->data.smenu.tx, state->data.smenu.ty, 11,
		  state->data.smenu.amount_of_entries + 2, 0, 0, 0);
	for (i = 0; i < state->data.smenu.amount_of_entries; i++)
		draw_string(&game->dd, state->data.smenu.tx + 1,
			    state->data.smenu.ty + 1 + i,
			    state->data.smenu.entries[i], 8);
	if (state->is_updating)
		draw_rect(&game->dd, state->data.smenu.tx + 10,
			  state->data.smenu.ty + 1 +
				state->data.smenu.selected_entry,
			  1, 1, 255, 255, 255);
}
