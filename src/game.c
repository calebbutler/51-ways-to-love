#include <stdbool.h>
#include <SDL2/SDL.h>

#include "menu.h"
#include "textbox.h"
#include "draw.h"
#include "game.h"

bool game_init(struct game_data *game)
{
	game->stackp = game->stack;
	game->is_running = true;
	if (!draw_init(&game->dd, 2))
		return false;
	return true;
}

void game_event_handling(struct game_data *game)
{
	if (SDL_PollEvent(&game->event)) {
		if (game->event.type == SDL_QUIT) {
			game->is_running = false;
			return;
		}
		switch (game->stackp->type) {
		case STATE_TYPE_SMALL_MENU:
			small_menu_handle_events(game, game->stackp);
			break;
		case STATE_TYPE_TEXTBOX:
			textbox_handle_events(game, game->stackp);
			break;
		}
	}
}

void game_update(struct game_data *game)
{
	int i;
	for (i = 0; i <= game->stackp - game->stack; i++) {
		if (game->stack[i].is_updating) {
			switch (game->stack[i].type) {
			case STATE_TYPE_SMALL_MENU:
			case STATE_TYPE_TEXTBOX:
				break;
			}
		}
	}
}

void game_draw(struct game_data *game)
{
	int i;
	draw_clear(&game->dd);
	for (i = 0; i <= game->stackp - game->stack; i++) {
		if (game->stack[i].is_rendering) {
			switch (game->stack[i].type) {
			case STATE_TYPE_SMALL_MENU:
				small_menu_draw(game, game->stack + i);
				break;
			case STATE_TYPE_TEXTBOX:
				textbox_draw(game, game->stack + i);
				break;
			}
		}
	}
	draw_present(&game->dd);
}

void game_cleanup(struct game_data *game)
{
	draw_cleanup(&game->dd);
}

void game_state_stack_unsafe_pop(struct game_data *game)
{
	game->stackp--;
}

bool game_state_stack_safe_pop(struct game_data *game)
{
	if (game->stackp - game->stack <= 0)
		return false;
	game_state_stack_unsafe_pop(game);
	return true;
}

void game_state_stack_pop(struct game_data *game)
{
	if (game->stackp - game->stack <= 0) {
		SDL_Log("state stack underflow!\n");
		game->is_running = false;
	} else {
		game_state_stack_unsafe_pop(game);
	}
}

void game_state_stack_push(struct game_data *game)
{
	if (game->stackp - game->stack >= 7) {
		SDL_Log("state stack overflow!\n");
		game->is_running = false;
	} else {
		game->stackp++;
		game->stackp->is_updating = true;
		game->stackp->is_rendering = true;
	}
}

void game_state_stack_update_and_render_top(struct game_data *game)
{
	game->stackp->is_updating = true;
	game->stackp->is_rendering = true;
}
