#include <stdbool.h>
#include "menu.h"
#include "textbox.h"
#include "game.h"

int main(void)
{
	struct game_data game;

	if (!game_init(&game))
		goto cleanup;

	game.stackp->type = STATE_TYPE_TEXTBOX;
	textbox_setup(&game.stackp->data.textbox,
"Youre Late Typical Lib Always",
"wasting time and wasting dimes",
"I should call you President",
NULL);

	game_state_stack_update_and_render_top(&game);

	while (game.is_running) {
		game_event_handling(&game);
		game_update(&game);
		game_draw(&game);
	}
cleanup:
	game_cleanup(&game);
	return 0;
}
