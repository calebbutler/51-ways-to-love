#include <stdbool.h>
#include <stdint.h>
#include <SDL2/SDL.h>
#include "draw.h"

bool draw_init(struct draw_data *dd, int scalar)
{
	dd->window = NULL;
	dd->screen = NULL;
	dd->tile_map = NULL;
	dd->scalar = scalar;

	dd->bg_color_r = 200;
	dd->bg_color_g = 200;
	dd->bg_color_b = 200;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		SDL_Log("SDL_Init error: %s\n", SDL_GetError());
		return false;
	}

	dd->window = SDL_CreateWindow("rpg", SDL_WINDOWPOS_UNDEFINED,
				      SDL_WINDOWPOS_UNDEFINED, 256 * dd->scalar,
				      256 * dd->scalar, SDL_WINDOW_SHOWN);
	if (dd->window == NULL) {
		SDL_Log("SDL_CreateWindow error: %s\n", SDL_GetError());
		return false;
	}

	dd->screen = SDL_GetWindowSurface(dd->window);
	if (dd->screen == NULL) {
		SDL_Log("SDL_GetWindowSurface error: %s\n", SDL_GetError());
		return false;
	}

	dd->tile_map = SDL_LoadBMP("res/tile_map.bmp");
	if (dd->tile_map == NULL) {
		SDL_Log("SDL_LoadBMP error: %s\n", SDL_GetError());
		return false;
	}

	return true;
}

void draw_cleanup(struct draw_data *dd)
{
	SDL_FreeSurface(dd->tile_map);
	SDL_DestroyWindow(dd->window);
	SDL_Quit();
}

void draw_clear(struct draw_data *dd)
{
	if (SDL_FillRect(dd->screen, NULL,
			 SDL_MapRGB(dd->screen->format, dd->bg_color_r,
				    dd->bg_color_g, dd->bg_color_b)) < 0) {
		SDL_Log("SDL_FillRect error: %s\n", SDL_GetError());
	}
}

void draw_present(struct draw_data *dd)
{
	if (SDL_UpdateWindowSurface(dd->window) < 0) {
		SDL_Log("SDL_UpdateWindowSurface error: %s\n", SDL_GetError());
	}
}

void draw_rect(struct draw_data *dd, int tx, int ty, int tw, int th,
	       uint8_t color_r, uint8_t color_g, uint8_t color_b)
{
	SDL_Rect rect = {tx * 8 * dd->scalar, ty * 8 * dd->scalar,
			 tw * 8 * dd->scalar, th * 8 * dd->scalar};
	if (SDL_FillRect(dd->screen, &rect,
			 SDL_MapRGB(dd->screen->format, color_r,
				    color_g, color_b)) < 0) {
		SDL_Log("SDL_FillRect error: %s\n", SDL_GetError());
	}
}

void draw_tile(struct draw_data *dd, int tx, int ty, int t_id)
{
	SDL_Rect srcrect = {t_id % 8 * 8, t_id / 8 * 8, 8, 8};
	SDL_Rect dstrect = {tx * 8 * dd->scalar, ty * 8 * dd->scalar,
			    8 * dd->scalar, 8 * dd->scalar};
	if (SDL_BlitScaled(dd->tile_map, &srcrect, dd->screen, &dstrect) < 0)
		SDL_Log("SDL_BlitScaled error: %s\n", SDL_GetError());
}

void draw_char(struct draw_data *dd, int tx, int ty, char c)
{
	if (c - 'A' >= 0 && c - 'A' < 26)
		draw_tile(dd, tx, ty, c - 'A');
	else if (c - 'a' >= 0 && c - 'a' < 26)
		draw_tile(dd, tx, ty, c - 'a');
}

void draw_string(struct draw_data *dd, int tx, int ty, char *string,
		 int string_length)
{
	int i;
	for (i = 0; string[i] != '\0' && i < string_length; i++)
		draw_char(dd, tx + i, ty, string[i]);
}
