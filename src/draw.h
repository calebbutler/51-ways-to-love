#ifndef _DRAW_H
#define _DRAW_H

#include <stdbool.h>
#include <stdint.h>
#include <SDL2/SDL.h>

struct draw_data {
	SDL_Window *window;
	SDL_Surface *screen;
	SDL_Surface *tile_map;
	int scalar;

	uint8_t bg_color_r;
	uint8_t bg_color_g;
	uint8_t bg_color_b;
};

bool draw_init(struct draw_data *dd, int scalar);
void draw_cleanup(struct draw_data *dd);
void draw_clear(struct draw_data *dd);
void draw_present(struct draw_data *dd);
void draw_rect(struct draw_data *dd, int tx, int ty, int tw, int th,
	       uint8_t color_r, uint8_t color_g, uint8_t color_b);
void draw_tile(struct draw_data *dd, int tx, int ty, int t_id);
void draw_char(struct draw_data *dd, int tx, int ty, char c);
void draw_string(struct draw_data *dd, int tx, int ty, char *string,
		 int string_length);

#endif /* _DRAW_H */
