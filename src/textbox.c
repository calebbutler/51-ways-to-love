#include "textbox.h"
#include "game.h"

void textbox_setup(struct textbox *tb, char *string1, char *string2,
		   char *string3,
		   void (*callback)(struct game_data *game,
				    struct state *state))
{
	int i;

	tb->callback = callback;
	for (i = 0; i < 30; i++) {
		tb->text1[i] = string1[i];
		if (string1[i] == '\0')
			break;
	}
	for (i = 0; i < 30; i++) {
		tb->text2[i] = string2[i];
		if (string2[i] == '\0')
			break;
	}
	for (i = 0; i < 30; i++) {
		tb->text3[i] = string3[i];
		if (string3[i] == '\0')
			break;
	}
}

void textbox_handle_events(struct game_data *game, struct state *state)
{
	if (game->event.type == SDL_KEYDOWN && game->event.key.repeat == 0 &&
	    game->event.key.keysym.sym == SDLK_RIGHT)
		state->data.textbox.callback(game, state);
}

void textbox_draw(struct game_data *game, struct state *state)
{
	draw_rect(&game->dd, 0, 27, 32, 5, 0, 0, 0);
	draw_string(&game->dd, 1, 28, state->data.textbox.text1, 30);
	draw_string(&game->dd, 1, 29, state->data.textbox.text2, 30);
	draw_string(&game->dd, 1, 30, state->data.textbox.text3, 30);
}
