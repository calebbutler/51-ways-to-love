#ifndef _TEXTBOX_H
#define _TEXTBOX_H

struct state;
struct game_data;

struct textbox {
	void (*callback)(struct game_data *game, struct state *state);
	char text1[30];
	char text2[30];
	char text3[30];
};
void textbox_setup(struct textbox *tb, char *string1, char *string2,
		   char *string3,
		   void (*callback)(struct game_data *game,
				    struct state *state));
void textbox_handle_events(struct game_data *game, struct state *state);
void textbox_draw(struct game_data *game, struct state *state);

#endif /* _TEXTBOX_H */
